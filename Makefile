build: clean
	zip -r take_diversion.zip ./ -x *.git* README.* Makefile LICENSE
	mkdir build && mv take_diversion.zip build

clean:
	rm -rf ./build
