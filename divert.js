/* global variables */
var pattern = "https://www.youtube.com/watch?*";
var redirect_domain = "www.invidio.us";
var enabled = true;

function notify(source, target) {
  var title = browser.i18n.getMessage("notificationTitle");
  var content = browser.i18n.getMessage("notificationContent", [source, target]);
  browser.notifications.create({
    "type": "basic",
    "iconUrl": browser.extension.getURL("icons/enabled/64x64.png"),
    "title": title,
    "message": content
  });
}

/* Intercept Youtube Traffic */
function redirect(requestDetails) {
  url = new URL(requestDetails.url);
  notify(url.host, redirect_domain);
  url.host = redirect_domain;

  // console.log("addon enabled: ", url.toString());

  return {
    redirectUrl: url.toString() 
  }
}

/* Activate/Deactivate */
function toggleState() {
  if (enabled == false) {
	browser.webRequest.onBeforeRequest.addListener(redirect, {urls: [pattern]},["blocking"]);
	browser.browserAction.setIcon({"path": "icons/enabled/48x48.png"});
	enabled = true;
  } else {
	  browser.webRequest.onBeforeRequest.removeListener(redirect);
	  browser.browserAction.setIcon(
		  {"path": "icons/disabled/48x48.png"});
	  enabled = false;
  }
  //console.log("Is addon enabled? ", enabled);
}

browser.webRequest.onBeforeRequest.addListener(redirect, {urls: [pattern]},["blocking"]);
browser.browserAction.onClicked.addListener(toggleState)
