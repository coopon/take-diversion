### Take Diversion

Web extension (addon) for Firefox that diverts users to watch youtube video on a invidious instance.

### Installing Addon

Firefox users can install the add-on from [here](https://addons.mozilla.org/en-US/firefox/addon/take-diversion/)

### Development

Firefox doesn't allow add-ons to be installed without their cryptographic signature, for this reason the addon has to be submitted to https://addons.mozilla.org. For development purposes Firefox allows to load addons temporarily. To load a add-on temporarily, type `about:debugging` in address bar and choose `This Firefox`

The extension is a Free Software licensed under GNU GPL v3.0
